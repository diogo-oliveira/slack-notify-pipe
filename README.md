# Bitbucket Pipelines Pipe: Slack Notify

Este pipe envia notificação no Slack

## Definição de YAML

Adicione o seguinte trecho à seção de script do seu arquivo `bitbucket-pipelines.yml`:

```yaml
script:
  - pipe: diogo0liveira/slack-notify-pipe:1.0.0
    variables:
      SLACK_URL: "<string>"
      # PAYLOAD: "<string>" # Optional
      # MESSAGE: "<string>" # Optional
      # DEBUG: "<boolean>" # Optional
```
## Variáveis

| Variável Uso          | Descrição                                                                                                       |
| --------------------- | --------------------------------------------------------------------------------------------------------------- |
| SLACK_URL (*)         | URL de entrada do Webhook. É recomendável usar uma variável de repositório seguro                               |
| PAYLOAD               | String json codificada no padrão de mensagem do Slack. Payload tem prioridade em relação ao messagem. 
|						| Mais sobre o jq: [aqui](https://stedolan.github.io/jq/). Mais sobre como compor mensagem para o slack:[aqui](https://api.slack.com/messaging/composing)
| MESSAGE               | Mensagem dA notificação                                                                                         |
| DEBUG                 | Turn on extra debug information. Default: `false`.                                                              |

_ (*) ​​= variável necessária._

## Pré-requisitos

## Exemplos

Exemplo básico:

```yaml
script:
  - pipe: diogo0liveira/slack-notify-pipe:1.0.0
    variables:
      MESSAGE: "Mensagem de notificação"
      SLACK_URL: "${SLACK_URL}"
```

Exemplo do Arquivo de Payload:

```bash
'{
	"blocks": [
		{
			"type": "context",
			"elements": [
				{
					"type": "mrkdwn",
					"text": Autor: Diogo Oliveira
				}
			]
		},
		{
			"type": "divider"
		},
		{
			"type": "section",
			"text": {
				"type": "plain_text",
				"text": Mensagem da notificação,
				"emoji": true
			}
		}
	]
}'
```

## Apoio, suporte
Se você precisar de ajuda com este canal ou tiver um problema ou solicitação de recurso, informe-nos.
O tubo é mantido por diogo0liveira@hotmail.com.

Se você estiver relatando um problema, inclua:

- a versão do pipe
- logs e mensagens de erro relevantes
- Passos para reproduzir
