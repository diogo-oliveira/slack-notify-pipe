#!/usr/bin/env bash
#
# Este pipe envia notificação no slack
#

source "$(dirname "$0")/common.sh"

SLACK_URL=${SLACK_URL:?'SLACK_URL não informado.'}

info "Enviando notificação para o Slack ..." 

if [ -n "${PAYLOAD}" ]
  then
    debug PAYLOAD: "${PAYLOAD}"
elif [ -n "${MESSAGE}" ] 
  then
    debug MESSAGE: "${MESSAGE}"

    PAYLOAD=$(jq -n \
      --arg MESSAGE "${MESSAGE}" \
      --arg BITBUCKET_WORKSPACE "${BITBUCKET_WORKSPACE}" \
      --arg BITBUCKET_REPO_SLUG "${BITBUCKET_REPO_SLUG}" \
      --arg BITBUCKET_BUILD_NUMBER "${BITBUCKET_BUILD_NUMBER}" \
    '{ attachments: [
      {
        "fallback": $MESSAGE,
        "color": "#439FE0",
        "pretext": "Notification sent from <https://bitbucket.org/\($BITBUCKET_WORKSPACE)/\($BITBUCKET_REPO_SLUG)/addon/pipelines/home#!/results/\($BITBUCKET_BUILD_NUMBER)|Pipeline #\($BITBUCKET_BUILD_NUMBER)>",
        "text": $MESSAGE,
        "mrkdwn_in": ["pretext"]
      }
    ]}')
  else
	  fail "informe payload ou messagem para a notificação!"
fi

[ "${DEBUG}" == "true" ] && EXTRA_ARGS="--verbose" || EXTRA_ARGS=""
CURL_OUTPUT="/tmp/pipe-$RANDOM.txt"

run curl -s -X POST --output ${CURL_OUTPUT} -w "%{http_code}" \
  -H "Content-Type: application/json" \
  -d "${PAYLOAD}" \
  ${EXTRA_ARGS} \
  ${SLACK_URL}

RESPONSE=$(cat ${CURL_OUTPUT})
info "HTTP Response: $(echo ${RESPONSE})"

if [[ "${RESPONSE}" = "ok" ]]; then
  success "Slack Notify executado com sucesso!"
else
  fail "Slack Notify falhou! :("
fi
