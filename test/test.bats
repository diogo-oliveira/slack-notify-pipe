#!/usr/bin/env bats

setup() {
  DOCKER_IMAGE=${DOCKER_IMAGE:="test/slack-notify"}

  # echo "Building image..."
  # docker build -t ${DOCKER_IMAGE}:test .
}

@test "Dummy test" {
#  run docker run --rm \
#   -e MESSAGE="Pipelines is awesome!" \
#   -e WEBHOOK_URL=$SLACK_WEBHOOK \
#   -e BITBUCKET_WORKSPACE="$BITBUCKET_WORKSPACE" \
#   -e BITBUCKET_REPO_SLUG="$BITBUCKET_REPO_SLUG" \
#   -e BITBUCKET_BUILD_NUMBER="$BITBUCKET_BUILD_NUMBER" \
#   -v $(pwd)/tmp:/tmp \
#   -v $(pwd):$(pwd) \
#   -w $(pwd) \
#   ${DOCKER_IMAGE}

  run docker build -t ${DOCKER_IMAGE}:test .

  echo "Status: $status"
  echo "Output: $output"

  [ "$status" -eq 0 ]

 # request=$(cat $(pwd)/tmp/pipe-*.txt)

 # [ "$request" = "ok" ]
}

teardown() {
  rm -rf $(pwd)/tmp/pipe-*.txt
}